# Initialize the server
import couchdb
couch_server = couchdb.Server('http://admin:admin@172.28.101.47:5984/')

# Create a database
created_db = couch_server.create('test')

# Insert a document
doc = {'foo': 'bar'}
doc_id, doc_rev = created_db.save(doc)

# Retrieve the document, modifying it and then updating it in the databse
new_doc = created_db[doc_id]
new_doc['foo'] = 'faa'
created_db[new_doc.id] = new_doc

# Delete the document and the database
created_db.delete(new_doc)
del couch_server['test']
# We could also delete the datasebe with
# couch_server.delete('test')
