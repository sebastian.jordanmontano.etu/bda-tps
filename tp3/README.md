# TP3

## Une api simple géographique

### Trouver grâce à l’API la ville la moins peuplé de la région Auvergne-Rhones-Alpes.

Le champ `boost` ça ne marche pas avec la region. Alors, cette question n'est pas possible de la faire.

### Trouver grâce à l’API, la ville la plus peuplé dont le nom débute par “Mon”

Avec la requête `curl -s "https://geo.api.gouv.fr/communes?nom=Mon&boost=population&limit=1"| jq`

On obtient:

```json
[
  {
    "nom": "Montpellier",
    "code": "34172",
    "codeDepartement": "34",
    "siren": "213401722",
    "codeEpci": "243400017",
    "codeRegion": "76",
    "codesPostaux": [
      "34070",
      "34000",
      "34090",
      "34080"
    ],
    "population": 295542,
    "_score": 0.11570496625914721
  }
]
```

### À l’aide du programme jq et de l’API, sauvegarder dans un document nord.json les informations pour toutes les villes du Nord et leur code postal

Avec la requête:

```bash
curl -s "https://geo.api.gouv.fr/departements/59/communes" | jq > nord.js
```

On obtient le resultat.

## Installer CouchDB

Pour installer `couchdb` il faut suivres les instructions d'installation de la doc de couchdb: https://docs.couchdb.org/en/3.2.2-docs/install/unix.html

Après l'avoir installé, on peut tester d'accerder depuis notre machine à nous, dehors de la machine virtuelle.

```bash
curl 172.28.101.47:5984 | jq
```

![](./img/1.png)

## Manipulation simple

Pour créer una base de données, depuis notre ornidateur, il faut faire:

```bash
curl -u admin:admin -X PUT 172.28.101.47:5984/demo
```

Après, on peut ajouter une donnée avec la cle `chouquette` avec la commande:

```bash
curl -u admin:admin -X PUT --data '{"pain":["au", "chocolat"]}' 172.28.101.47:5984/demo/chouquette
```

![](./img/2.png)

```bash
LeRoiCrimson:bda-tps sebastian$ curl -u admin:admin -X PUT --data '{"pain":["au", "chocolat"]}'     172.28.101.47:5984/demo/chouquette
{"ok":true,"id":"chouquette","rev":"1-ae190de59cdf3ff22b50a3fdee924c24"}
```

Pour le supprimer:

```bash
curl -u admin:admin -X DELETE  "172.28.101.47:5984/demo/chouquette?rev=1-ae190de59cdf3ff22b50a3fdee924c24"
```

![](./img/3.png)


## Interaction programmatique

Le fichier interaction_programmatique se trouve dans ce même dossier [interaction_programmatique.py](./interaction_programmatique.py)

```python

# Initialize the server
import couchdb
couch_server = couchdb.Server('http://admin:admin@172.28.101.47:5984/')

# Create a database
created_db = couch_server.create('test')

# Insert a document
doc = {'foo': 'bar'}
doc_id, doc_rev = created_db.save(doc)

# Retrieve the document, modifying it and then updating it in the databse
new_doc = created_db[doc_id]
new_doc['foo'] = 'faa'
created_db[doc_id] = new_doc

# Delete the document and the database
created_db.delete(doc)
del couch_server['test']
# We could also delete the datasebe with
# couch_server.delete('test')
```

### Inserter toutes les données de [nord.js](./nord.js) dans une ville par documents

```py
import json
import couchdb


couch_server = couchdb.Server('http://admin:admin@172.28.101.47:5984/')
created_db = couch_server.create('villes')

file = open('./nord.json')
data = json.load(file)

for d in data:
    created_db.save(d)
```
