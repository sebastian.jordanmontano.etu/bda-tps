import json
import couchdb


couch_server = couchdb.Server('http://admin:admin@172.28.101.47:5984/')
villes_db = couch_server['villes']

selector_name_starts_with_mon = {
    'selector': {
        'nom': {'$regex': 'Mon.*'}
    },
    'sort': [{'population': 'desc'}]
}

for doc in villes_db.find(selector_name_starts_with_mon):
    print(doc)
