# Travail Practique 2

## Installation `redis`

- Créer una nouvel machine virtual `redis`
- Ajouter la clé privée ssh à la machine virtuelle
- Installer `redis` selon la documentation

**_Le mot de passe est: `ubuntu`_**

- Après, permettres les accès en distante en modifiant ces ligne  dans le fichier `redis.conf`:
![](./img/redis_conf_file.png)

- Utiliser `redis-cli` pour tester de commandes.

Avant, il faut redémarrer le serveurs avec l'aide de commandes:

`sudo service redis-server stop`

`sudo service redis-server start`

Après d'utiliser les commandes de tests qu'il y a dans le tp, on peut voir le résultat suivant:
![](./img/trying-first-commandes.png)

La commande `SELECT` serve pour changer la database. Le paramètre est l'index de la base de données.

![](./img/select-command-explanation.png)

## Utilisation simple de Redis

Screenshot de l'utilisation de telenet sur une machine linux. J'utilise mac et pour tester telnet j'ai utilisé une machine Lunux

![](./img/telnet.png)

## Utilisation applicative de Redis

Après installer `redis` en utilisant de `pip3`

![](./img/python-simple.png)

Je n'envoie pas de mot de pass parce que j'ai configuré ma machine virtuelle pour ne pas demander le mot de passe.

Pour ajouter de fichiers binaires:

```python
with open("./img/pharo-logo.png", "rb") as f:
    redis_client["pharo-logo"] = f.read()
```