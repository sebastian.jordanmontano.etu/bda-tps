import redis

redis_client = redis.Redis(host="172.28.100.223", port=6379, db=0)

with open("./img/pharo-logo.png", "rb") as f:
    redis_client["pharo-logo"] = f.read()

print(redis_client["pharo-logo"])
