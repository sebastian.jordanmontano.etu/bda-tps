# Travail Pratique I

## Adresse IP de la machine

`172.28.100.143`

## Utilisation d’OpenStack

Les clés ssh ajoutées correctement.

![](./img/ssh-key-ajoute.png)

La machine virtuelle avec les moins de ressources.

![](./img/machine-virtuelle-creee.png)

Pour créer le compte personal pour `cha`, on exécute le command: `sudo adduser cha`.
Pour ajouter la clé ssh, il faut éditer le fichier `./.ssh/authorized_keys`.

## Installation et configuration des accès au serveur PostgreSQL

### Se connecter à une base de donnée via une VM

- Pour se connecter à la vm, exécuter cette commande depuis la terminal local :
`$ ssh ubuntu@172.28.100.143`
    - Aussi c'est possible d'éditer le fichier `.ssh/config` pour se connecter directement en ajoutant:
    ```bash
    Host bda
	User ubuntu
	Hostname 172.28.100.143
	IdentityFile /Users/Sebastian/.ssh/cloud
    ```
- Pour se connecter à la bd microblog de l'utilisateur `ubuntu`: 
`$ ubuntu@tp1:~$ sudo -i -u redaid psql -d microblog`. Mon utilisateur est `ubuntu`

### Configuration d'accès au bd

1. Se connecter à la vm -> ubuntu user
2. passez les commandes suivantes:
`$ cd ../..`
`$ cd etc/postgresql/14/main` (la version de postgres peut varier)
3. Les fichiers de config se trouvent dedans:
`$ ll`
4. Ajouter les hôtes dans `pg_hba.conf`
`$ sudo nano .pg_hba.conf`
et ajouter la ligne: 
5. Modifiez postgresql.conf
`$ sudo nano postgresql.conf`
et Changer la ligne: `listen_addresses = '*'`
6. arreter le serveur postgres
`$ sudo service postgresql stop`
7. redémarrer le serveur postgres
`$ sudo service postgresql start`
Aussi c'est possible d'exécuter la commande `$ sudo service postgresql restart`

###  Create a user with superuser priviliges

- `$ ubuntu@tp1:~$ sudo -i -u postgres`
- `postgres@tp1:~$ createuser username  --interactive`
- `postgres@tp1:~$ psql`
- `postgres=# alter user cha with encrypted password 'ILovePostgreSQL';`

## Une application de micro-blogging

- Télécharger le fichier du [schéma sql](https://paperman.name/page/enseignement/2021/nosql/td1/microblog.sql)
- Créer una base de donnée appelée `microblog` avec la commande `CREATE DATABASE microblog;`
- Installer le schéma, depuis la machine personnelle pas dedans la vm, avec la commande: `cat ~/Downloads/microblog.sql | psql -h $remoteip -U $remote --dbname microblog`

### Est-ce qu’il est possible pour l’utilisateur common_user:

- de récupérer la totalité des messages publiés

Non, il n'est pas possible. On peut voir dans la dernière ligne `GRANT SELECT ON messages to common_user;`
L'authrisation est fait pour la `VIEW messages` et ne pas pour la table `messages_store`. Alors, l'utilisateur commun
ne peut voir que les messages dont lui a participé.

- de récupérer la liste des utilisateurs inscrits

Non, il ne peut pas. Il n'y a pas des droit de `select` pour la table d'utilisateurs.

- de récupérer le mot de passe d’un utilisateur inscrit

Pour récupérer le mot de passe d'un utilisateur, il faut avoir acces de lecture à la table `user_store`.
Pour `common_user` il n'y a pas des droits à cette table-là alors ce n'est pas possible.

### Est-il possible pour l’utilisateur possédant la base de données:

- de récupérer la totalité des messages publiés
- de récupérer la liste des utilisateurs inscrits

La réponse aux deux questions est oui. L'utilisateur a l'accèss alors c'est possible pour lui.

- de récupérer le mot de passe d’un utilisateur inscrit

Si le mot de pass est encypté avec una fonction hash `crypt($2, gen_salt('bf'))` alors ce n'est pas possible de récuperer le mot de passe. On peut que vérifier si le mot de passe qui a été saisi correspond lequel est dans la base de données.

### A quoi sert la ligne:

```sql
CLUSTER messages_store USING idx_pub_date;
```

Ça sert à grouper les messages par la date de publication.
